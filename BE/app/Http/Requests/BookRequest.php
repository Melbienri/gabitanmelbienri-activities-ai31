<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name' => ['required', 'bail', 'max:255'],
            'author' => ['required', 'bail', 'max:255'],
            'copies' => ['required', 'integer'],
            'category_id' => ['exists:categories,id', 'required', 'integer']
        ];
    }
}
