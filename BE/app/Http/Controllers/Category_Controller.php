<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

/**
 * @group Categories Controller
 */
class Category_Controller extends Controller
{
    public function index(){
        $category = Category::all();
        return response()->json($category);
    }
}
