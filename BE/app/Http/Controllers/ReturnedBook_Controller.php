<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use App\Http\Requests\ReturnedBookRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use function PHPUnit\Framework\isEmpty;

class ReturnedBook_Controller extends Controller
{
    public function index()
    {
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->get());
    }

    public function store(ReturnedBookRequest $request)
    {
        $borrowedBook = BorrowedBook::where([
            ['book_id', $request->book_id],
            ['patron_id', $request->patron_id],
        ])->firstOrFail();
        
        if(!empty($borrowedBook))
        {
            if($borrowedBook->copies == $request->copies){
                $borrowedBook->delete();
            }
            else
            {
                $borrowedBook->update(['copies' => $borrowedBook->copies - $request->copies]);
            }   
            
            $create_returned = ReturnedBook::create($request->only(['book_id', 'copies', 'patron_id']));
            $returnedBook = ReturnedBook::with(['book'])->find($create_returned->id);
            $copies = $returnedBook->book->copies + $request->copies;
        
            $returnedBook->book->update(['copies' => $copies]);
            return response()->json(['message' => 'Book returned successfully!']);
        }  

    }

    public function show($id)
    {
    try{
        $returnedBook = ReturnedBook::with(['book', 'book.category', 'patron'])->findOrfail($id);
        return response()->json($returnedBook); 
    }catch(ModelNotFoundException $exception){
        return response()->json(['message' => 'Returned book not found']);
    }
    }
}
