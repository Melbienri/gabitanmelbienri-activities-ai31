<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\BookRequest;

class Book_Controller extends Controller
{
    public function index(){
        return response()->json(Book::with(['category:id, category'])->get());
    }
    public function store(BookRequest $request){
        return response()->json(Book::create($request->all()));
    }
    public function show($id){
    try{
        $book = Book::with(['category:id, category'])->where('id', $id)->firstOrFail();
        return response()-> json($book);
    }catch(ModelNotFoundException $exception){
        return response()->json(['message' => 'Book not found']);
    }
    }
    public function updateBook(BookRequest $request, $id){
        $book = Book::with(['category:id, category'])->where('id', $id)->firstOfFail();
        $book->update($request->all());
        return response()->json(['message' => 'Book was updated successfully', 'book' => $book]);
    }
    public function deleteBook($id)
    {
    try{
        $book = Book::where('id', $id)->firstOrFail();
        $book->delete();
        return response()->json(['message' => 'Book deleted successfully!']);
    }catch(ModelNotFoundException $exception){
        return response()->json(['message' => 'Book not found']);
    }
    }
}
