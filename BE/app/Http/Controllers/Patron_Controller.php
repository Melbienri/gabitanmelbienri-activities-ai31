<?php

namespace App\Http\Controllers;

use App\Models\Patron;
use App\Http\Requests\PatronRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Patron_Controller extends Controller
{
    public function index()
    {
        return response()->json(Patron::all());
    }
    public function store(PatronRequest $request)
    {
        return response()->json(Patron::create($request->all()));
    }
    public function show($id)
    {
        return response()->json(Patron::findOrFail($id));
    }
    public function update(PatronRequest $request, $id)
    {
    try{
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->update($request->all());
        return response()->json(['message' => 'Patron updated','patron'=>$patron]);
    }catch(ModelNotFoundException $exception){
        return response()->json(['message' => 'Patron not found']);
    }
    }
    public function destroy($id)
    {
    try{
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->delete();
        return response()->json(['message' => 'Patron deleted successfully!']);
    }catch(ModelNotFoundException $exception){
        return response()->json(['message' => 'Patron not found']);
    }
    }
}
