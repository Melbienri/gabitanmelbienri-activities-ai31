<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\BorrowedBookRequest;

class BorrowedBook_Controller extends Controller
{
    public function index(){
        return response()->json(BorrowedBook::with([
            'patron', 'book', 'book.category'
        ])->get());
    }
    public function show($id)
    {
    try{
        $borrowedbook = BorrowedBook::with(['patron', 'book', 'book.category'])->where('id', $id)->firstOrFail();
        return response()->json($borrowedbook);
    }catch(ModelNotFoundException $exception){
        return response()->json(['message' => 'Book not found']);
    }
    }
    public function save(BorrowedBookRequest $request){
        $create_borrowed = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
        
        $borrowedbook = BorrowedBook::with(['book'])->find($create_borrowed->id);
        $copies = $borrowedbook->book->copies - $request->copies;
        $borrowedbook->book->update(['copies' => $copies]);

        return response()->json(['message' => 'Book borrowed successfully', 'borrowedbook' => $borrowedbook]);
    }
}
