<?php

use App\Http\Controllers\Book_Controller;
use App\Http\Controllers\Patron_Controller;
use App\Http\Controllers\BorrowedBook_Controller;
use App\Http\Controllers\Category_Controller;
use App\Http\Controllers\ReturnedBook_Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/categories', [Category_Controller::class, 'index']);

Route::resource('/borrowedBook', BorrowedBook_Controller::class);
Route::resource('/returnedBook', ReturnedBook_Controller::class);

Route::apiResource('patrons', Patron_Controller::class);
Route::apiResource('books', Book_Controller::class);
