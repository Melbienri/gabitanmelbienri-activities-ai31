import Vue from 'vue'
import VueRouter from 'vue-router'
import Dash from '@/components/Pages/Dashboard/Dashboard.vue'
import Book from '@/components/Pages/Book/Book.vue'
import Patron from '@/components/Pages/Patron/Patron.vue'
import Settings from '@/components/Pages/Settings/Settings.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'dash',
        component: Dash
    },
    {
        path: '/Patron',
        name: 'patron',
        component: Patron
    },
    {
        path: '/Book',
        name: 'book',
        component: Book
    },
    {
        path: '/Settings',
        name: 'settings',
        component: Settings
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router