import vue from 'vue';
import vuex from 'vuex';

vue.use(vuex);

import patrons from './modules/patrons';
import books from './modules/books';
import borrowed from './modules/borrowed';
import returned from './modules/returned';

export default new vuex.Store({
    modules: {
        patrons, books, borrowed, returned
    }
})