var Bchart = document.getElementById('BarChart').getContext('2d');
var BarChart = new Chart(Bchart, {
    type: 'bar',
    data: {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
        datasets: [
            {
                label: 'Borrowed Books',
                data: [10, 10, 12, 9, 13],
                backgroundColor: '#4C57F3',
            },
            {
                label: 'Returned Books',
                data: [7, 11, 9, 10, 10],
                backgroundColor: '#4CA0F3',
            },
        ],
    },
    options: {
        responsive: true,
        legend: {
            position: 'top',
        }
    },
});

var Pchart = document.getElementById('PieChart').getContext('2d');
var PieChart = new Chart(Pchart, {
    type: 'pie',
    data: {
        labels: ['Programming', 'Fiction', 'Horror', 'Novel', 'Romance'],
        datasets: [
            {
                label: 'Borrowed Books',
                data: [20, 30, 10, 20, 20],
                backgroundColor: ['#4C57F3', '#0011FF', '#03256C', '#136EC8', '#4CA0F3']
            },
        ],
    },
    options: {
        responsive: true,
        legend: {
            position: 'top',
        }
    },
});